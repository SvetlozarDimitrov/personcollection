﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace PersonCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            var persons = new ObservableCollection<Person>();
            persons.CollectionChanged += CollectionChangedHandler;

            Task.Run(() =>
            {
                persons.Add(new Person
                {
                    FirstName = "Ivan",
                    LastName = "Ivanov",
                    DateOfBirth = DateTime.Now.AddYears(-40)
                });

                persons.Add(new Person
                {
                    FirstName = "Georgi",
                    LastName = "Georgiev",
                    DateOfBirth = DateTime.Now.AddYears(-20)
                });

                Thread.Sleep(8000);

                persons.Add(new Person
                {
                    FirstName = "Petar",
                    LastName = "Petrov",
                    DateOfBirth = DateTime.Now.AddYears(-30)
                });
            });

            Task.Run(() =>
            {
                Thread.Sleep(12000);
                persons.Remove();
            });

            Console.ReadKey();
        }

        private static void CollectionChangedHandler(object sender, CollectionChangedEventArgs<Person> e)
        {
            Console.WriteLine(e.Action == Action.UpdateCount ? $"Current count: ({e.Count}) {DateTime.Now}" :
                $"Action: {e?.Action}. First name: {e?.Item?.FirstName}. Last name: {e?.Item?.LastName}. Birth date: {e?.Item?.DateOfBirth}");
        }
    }
}