﻿using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace PersonCollection
{
    public enum Action
    {
        Add,
        Remove,
        UpdateCount
    }

    public class ObservableCollection<T>
    {
        public event CollectionChangedEventHandler CollectionChanged;
        public delegate void CollectionChangedEventHandler(object sender, CollectionChangedEventArgs<T> e);
        private const double notifyInterval = 5 * 1000;
        private static readonly object obj = new object();
        private SortedSet<T> _sortedSet;

        public ObservableCollection()
        {
            _sortedSet = new SortedSet<T>();
            SetNotificationTimer();
        }

        public void Add(T item)
        {
            lock(obj)
            {
                _sortedSet.Add(item);
                RaiseEvent(Action.Add, item);
            }
        }

        public void Remove()
        {
            lock(obj)
            {
                if (_sortedSet.Any())
                {
                    var maxItem = _sortedSet.Max;
                    _sortedSet.Remove(maxItem);
                    RaiseEvent(Action.Remove, maxItem);
                }
            }
        }

        private void RaiseEvent(Action action, T item = default) =>
          CollectionChanged?.Invoke(this, new CollectionChangedEventArgs<T> { Action = action, Item = item, Count = _sortedSet.Count });

        private void SetNotificationTimer()
        {
            var timer = new Timer();
            timer.Interval = notifyInterval;
            timer.Elapsed += (object sender, ElapsedEventArgs e) => RaiseEvent(Action.UpdateCount);
            timer.Start();
        }
    }
}