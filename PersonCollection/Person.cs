﻿using System;

namespace PersonCollection
{
    public interface IPerson 
    {
        string FirstName { get; }
        string LastName { get; }
        DateTime DateOfBirth { get; }
    }

    public class Person : IPerson, IComparable<IPerson>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public int CompareTo(IPerson obj)
        {
            return obj.DateOfBirth.CompareTo(this.DateOfBirth);
        }
    }
}