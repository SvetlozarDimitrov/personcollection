﻿using System;

namespace PersonCollection
{
    public class CollectionChangedEventArgs<T> : EventArgs
    {
        public T Item { get; set; }
        public Action Action { get; set; }
        public int Count { get; set; }
    }
}